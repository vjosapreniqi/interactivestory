package com.example.botek.interactivestory.model;

import com.example.botek.interactivestory.R;

public class Story {
    private Page[] mPages;


    public Story(){

        mPages = new Page[7];

        mPages[0] = new Page( R.drawable.page0,
        "Gjat kthimit nga udhëtimi për studimin e unazave të planenit Saturn, ju degjoni një" +
                "sinjal shqetësues që duket se vjen nga sipërgaqja e Marsit. Eshtë e quditshme sepse" +
                "nuk ka qenë ndonjë koloni 3 vitet e fundit. Madje edhe më e quditshme, të thërrasin në emër" +
                "\"Me ndihmo %1$s je shpresa ime e fundit!\"",
        new Choice("Ndalu dhe investigo", 1),
        new Choice("Vazhdo për në shtëpi në Tokë",2));

        mPages[1] = new Page(
                R.drawable.page1,
                "Me shkathtësi ndaleni anijen afer zonës nga vijnë zërat e quditshëm. Ju nuk keni hasur" +
                        "ndonjë gjë të quditshme gjat fluturimit, por është një shpellë përball jush. Prapa jush" +
                        "është një makinë hapsinore e braktisur që nga fillimi i shekullit 21.",
                new Choice("Eksploroje shpellën", 3),
                new Choice("Eksploroje makinën hapsinore", 4));

        mPages[2] = new Page(
                R.drawable.page2,
                    "Ju vazhdoni rrugetimin tuaj për në Tokë. Dy ditë më vonë, ju praoni një sinjal nga " +
                        "HQ që thotë se ata kanë detektuar një lloj anomalije në sipërfaqe të planetit Mars " +
                        "afër makinës hapsinore të braktisur. Ata ju kërkuan të investigosh, por zgjedhja është tërësisht " +
                        "e juaja sepse misioni juaj është zgjatur më tepër sesa që është palnifikuar dhe furnizimet " +
                        "janë tashmë të ulëta.",
                new Choice("Kthehu në Mars dhe investigo", 4),
                new Choice("Vazhdo për në shtëpi në Tokë", 6));

        mPages[3] = new Page(
                R.drawable.page3,
                        "Kostumi juaj EVA është i paisur me dritë, të cilën do ta përdorësh për navigimin e shpellës. " +
                        "Pas një kohe kërkimi, niveli i oksigjenit tuaj bie. Ju e dini se duhet ta mbushni rezervuarin tuaj, " +
                        "Por është një dritë mjaft e zbehtë përpara. ",
                new Choice("Mbushe reervuarin, eksploroje makinën hapsinore", 4),
                new Choice("Vazhdo drejt dritës së zbehtë", 5));

        mPages[4] = new Page(
                R.drawable.page4,
                    "Makina hapsinore është e mbuluar me pluhur dhe shumica e paneleve solare janë të prishura. Por ju jeni mjaft " +
                        "i befasuar kur shihni sistemin në bord të startojë dhe funksionojë. Në fakt, është një mesazh në ekran: " +
                        " \" %1$s, eja tek 28.543436, -81.369031.\" Këto koordinata nuk janë larg, por ju nuk e dini nëse oksigjeni " +
                        "do t'iu zgjat deri atje dhe për tu kthyer prapa.",
                new Choice("Eksploro koordinatat", 5),
                new Choice("Ktheu në Tokë", 6));

        mPages[5] = new Page(
                R.drawable.page5,
                "Pas një ecje të gjatë paksa të pjerrët, ju përfundoni sipër një krateri të vogël. Ju shikoni përrreth, " +
                        "dhe jeni shumë i lumtur të shihni android-in tuaj të preferuar, %1$s-S1124. Ai ka qenë i humbur nga misioni " +
                        "i kaluar në Mars! Ju e merni atë me vete në anijen tuaj dhe fluturoni për në Tokë.");

        mPages[6] = new Page(
                R.drawable.page6,
                "Ju arrini në Tokë. Përderisa misioni ishte një sukses, ju gjithnjë mendoni se nga kush dërgohej ai sinjal. " +
                        "Ndoshta në misionet tuaja të ardhme, ju do të jeni në gjendje të investigoni më shumë...");
            }


        public Page getPage(int pageNumber){
            return mPages[pageNumber];
        }

    }


