package com.example.botek.interactivestory.ui;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.botek.interactivestory.R;
import com.example.botek.interactivestory.model.Page;
import com.example.botek.interactivestory.model.Story;


public class StoryActivity extends ActionBarActivity {


    public static final String TAG = StoryActivity.class.getSimpleName();

    private Story mStory = new Story();
    private ImageView mImageView;
    private TextView mTextView;
    private Button mChoice1;
    private Button mChoice2;
    private String mName;
    private Page mCurrentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);
        Intent intent = getIntent();
        mName = intent.getStringExtra(getString(R.string.key_name));
        if(mName == null){
            mName ="Friend";
        }
        Log.d(TAG, mName);

        mImageView = (ImageView) findViewById(R.id.StoryImageView);
        mTextView = (TextView) findViewById(R.id.StoryTextView);
        mChoice1 =(Button) findViewById(R.id.ChoiceButton1);
        mChoice2 =(Button) findViewById(R.id.ChoiceButton2);
        loadPage(0);

    }
        private void loadPage(int choice){
        mCurrentPage = mStory.getPage(choice);

        Drawable drawable = getResources().getDrawable(mCurrentPage.getImageId());
        mImageView.setImageDrawable(drawable);

            String pageText = mCurrentPage.getText();

            //Shto emrin nese plaeholder-i eshte included. Nuk shtohet emri qe ska placeholder

            pageText= String.format(pageText, mName);


            mTextView.setText(pageText);

            if(mCurrentPage.isFinal()){

                mChoice1.setVisibility(View.INVISIBLE );// View.GONE for completely removing the layout
                mChoice2.setText("PLAY AGAIN");
                mChoice2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });

            }
            else{
            mChoice1.setText(mCurrentPage.getChoice1().getText());
            mChoice2.setText(mCurrentPage.getChoice2().getText());

            mChoice1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                int nextPage = mCurrentPage.getChoice1().getNextPage();
                loadPage(nextPage);

                }
            });

            mChoice2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int nextPage = mCurrentPage.getChoice2().getNextPage();
                    loadPage(nextPage);

                }
            });
        }
    }
}
